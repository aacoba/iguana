import time
import socket
import re
import imp
import sys
import traceback
import os
import thread
from IguanaExceptions import ModuleException as ModuleException

__author__ = 'aacoba@aacoba.net'


#TODO Debug Log
#TODO Rawlog


class irc():
    def __init__(self, address, nickname, port=6667, realname="A Python IRC Bot"):
        self.nickname = nickname
        print "Connecting to %s:%i as %s with realname: %s" % (address, port, nickname, realname)
        self.con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.con.connect((address, port))
        print self.con.recv(100000)
        self.con.send('NICK %s\r\n' % nickname)
        self.con.send('USER %s %s %s :%s\r\n' % (nickname, nickname, nickname, realname))

    def restart(self):
        self.disconnect("Restarting!")
        executable = sys.executable
        args = sys.argv[:]
        args.insert(0, sys.executable)

        time.sleep(1)
        print "Respawning!"
        os.execvp(executable, args)

    def cmdRestart(self, msgInfo=None, args=None):
        print msgInfo
        if msgInfo['ident'] == "aacoba" and msgInfo['hostname'] == "aacoba.net":
            self.sendUserMessage(msgInfo['user'], "Restarting the bot!")
            self.restart()

    def disconnect(self, message=None):
        if message is None:
            print "Quitting!"
            self.con.send('QUIT\r\n')
        else:
            print "Quitting with message: %s" % message
            self.con.send('QUIT :%s\r\n' % message)

    def joinChannel(self, channel, password=None):
        if password is None:
            print "Joining channel: #%s" % channel
            self.con.send('JOIN #%s\r\n' % channel)
        else:
            print "Joining channel: #%s with password: %s" % (channel, password)
            self.con.send('JOIN #%s %s\r\n' % (channel, password))

        goodJoinRe = "(?P<botFname>\S+!\S+@\S+) JOIN :#(?P<channame>.*)"
        badJoinRe = ":(?P<server>\S+) (?P<code>\d{3}) (?P<botname>\S+) (?P<channame>#\S+) :(?P<msg>.*)"

        #EH.addHook(goodJoinRe, self.joinChannelGoodFollowUp, (channel))
        #EH.addHook(badJoinRe, self.joinChannelGoodFollowUp, (channel, "Oops, Could not join channel: %s" % channel))

    def joinChannelGoodFollowUp(self, channame, message=None):
        if message is None:
            print "Channel %s joined!" % channame
        else:
            print "Could Not Join %s, %s" % (channame, message)
        EH.removeHook(r"(?P<botFname>\S+!\S+) JOIN :#%s" % channame)
        EH.removeHook(r":(?P<server>\S+) (?P<code>\d{3}) (?P<botname>\S+) #%s :(?P<msg>.*)" % channame)

    def leaveChannel(self, channel, message=None):
        if message is None:
            print "Leaving channel: #%s" % channel
            self.con.send("PART #%s\r\n" % channel)
        else:
            print "Leaving channel: #%s with message: %s" % (channel, message)
            self.con.send("PART #%s :%s\r\n" % (channel, message))

    def sendChannelMessage(self, channel, message):
        print ">> #{0}: {1}".format(channel, message)
        self.con.send("PRIVMSG #{0} :{1}\r\n".format(channel, message))

    def sendUserMessage(self, user, message):
        print ">> {0}: {1}".format(user, message)
        self.con.send("PRIVMSG {0} :{1}\r\n".format(user, message))

    def nickServAuth(self, password):  # TODO: DIT FIXEN
        print "Authing to nickserver using password: %s" % password
        self.con.send('PRIVMSG NickServ IDENTIFY %s\r\n' % password)
        resp = self.con.recv(1024)
        nickServRegex = re.compile(r":(?P<nickserv>NickServ!\S+@\S+) NOTICE (?P<botname>\S+) :(?P<message>.*)", re.IGNORECASE)
        if not nickServRegex.match(resp):
            print "Error! NickServ Response not valid!"
            print resp
            return False
        else:
            r = nickServRegex.search(resp)
            respGroupdict = r.groupdict()
            respMessage = respGroupdict['message']
            if respMessage == "Password accepted - you are now recognized.":
                print "Nickserv Authed!"
                print respMessage
                return True
            elif respMessage == "Password incorrect.":
                print "Nickserv Auth Failed!"
                print respMessage
                return False


class EventHandler():
    def __init__(self):
        self.commands = {}
        self.channelHooks = {}
        self.privMsgHooks = {}
        self.rawHooks = {}
        self.hookDict = {"raw": self.rawHooks, "private": self.privMsgHooks, "channel": self.channelHooks}

    # Event registration format
    # ['Type', 'rawregex']
    # Eventtypes:
    # * channel
    # * private
    # * raw

    def _getEventDict(self, eventType):
        try:
            EventDict = self.hookDict[eventType]
        except KeyError:
            raise ModuleException("Invalid eventtype used: {}".format(eventType))
        else:
            return EventDict



    def addHook(self, event, handler):
        try:
            eventType, eventRegex  = list(event)
        except ValueError:
            raise ModuleException("Invallid Event registration: No [type, eventregex] used")
        eventDict = self._getEventDict(eventType)
        try:
            eventDict[eventRegex]
        except KeyError:
            print "Adding {}hook on {} > {}".format(eventType, eventRegex, handler)
        else:
            print "Warning: Overwriting hook {} by {}".format(eventRegex, handler)
        eventDict[eventRegex] = handler

    def removeHook(self, event):
        try:
            eventType, eventRegex  = list(event)
        except ValueError:
            raise ModuleException("Invallid Event deletion: No [type, eventregex] used")
        eventDict = self._getEventDict(eventType)
        try:
            eventDict[eventRegex]
        except KeyError:
            raise ModuleException("Trying to remove non-existing hook on {}".format(eventRegex))
        else:
            print "Removing {}hook on {}".format(eventType, eventRegex)
            del eventDict[eventRegex]


    def checkForHook(self, eventType, text):
        eventDict = self._getEventDict()
        hits = []
        for hook in eventDict:
            r = re.compile(hook, re.IGNORECASE)
            if r.match(text):
                hits.append(hook)



    def getHook(self, hook):
        return self.commands.get(hook)


class DataHandler():
    def __init__(self):
        self.ServerPingRegex = re.compile(r"PING :(?P<server>\S+)")
        self.userPingRegex = re.compile(r":(?P<user>\S+)!\S+@\S+ PRIVMSG %s :\x01PING (?P<code>.*)\x01" % irc.nickname, re.IGNORECASE)
        self.channelMsgRegex = re.compile(r":(?P<user>\S+)!(?P<ident>\S+)@(?P<hostname>\S+) PRIVMSG #(?P<channel>\S+) :(?P<message>.*)", re.IGNORECASE)
        self.PrivMsgRegex = re.compile(r":(?P<user>\S+)!\S+\@\S+ PRIVMSG %s :(?P<message>.*)" % irc.nickname, re.IGNORECASE)

    def check(self, data):
        start_time = time.time()
        if self.ServerPingRegex.match(data):
            r = self.ServerPingRegex.search(data)
            rpl = r.groupdict()['server']
            print "Server Ping Received! ponging.."
            irc.con.send("PONG %s\r\n" % rpl)
        elif self.userPingRegex.match(data):
            pass
        elif self.channelMsgRegex.match(data):
            r = self.channelMsgRegex.search(data)
            chanMsgGroupDict = r.groupdict()
            user = chanMsgGroupDict['user']
            channel = chanMsgGroupDict['channel']
            message = chanMsgGroupDict['message']
            formattedMessage = "#%s: <%s> %s" % (channel, user, message)
            print formattedMessage
            hf = EH.checkForHook(message)
            if hf is not None:
                handler = hf[0]
                args = hf[1]
                messageInfo = chanMsgGroupDict
                try:
                    response = handler(irc, messageInfo, args)
                    if response is not None:
                        try:
                            spiltResponse = str(response).split('\r\n')
                            for line in spiltResponse:
                                irc.sendChannelMessage(channel, line)
                        except AttributeError:
                            irc.sendChannelMessage(channel, response)
                #except KeyError, e:
                #    irc.sendChannelMessage(channel, "Error while running {0}\r\n".format(handler))
                #    irc.sendChannelMessage(channel, "Error Caused by: {0}\r\n".format(formattedMessage))
                #    irc.sendChannelMessage(channel, "Error Message:{0}".format(e))
                #    irc.sendChannelMessage(channel, "Line: {0}".format(traceback.tb_lineno(sys.exc_info()[2])))
                except (NameError, KeyError, AttributeError, TypeError) as e:
                    if messageInfo['ident'] == "aacoba" and messageInfo['hostname'] == "aacoba.net":
                        errorTraceback = traceback.format_exc()
                        lines = errorTraceback.split('\n')
                        for line in lines:
                            irc.sendUserMessage(user, line)
                            #irc.sendChannelMessage(channel, line)
                    irc.sendChannelMessage(channel, "Something went wrong....")
            else:
                pass
        print "Handle took: {0}".format(time.time() - start_time)


class ModuleHandler():
    def __init__(self):
        self.botModules = {}

    def loadModule(self, name):
        #TODO Check for load function before loading
        #TODO Check for unload function before loading
        sys.path.append('modules/')
        try:
            mod = __import__(name)
        except ImportError as e:
            return e
            #!return "Error! No such module!"
        components = name.split('.')
        for comp in components[1:]:
            mod = getattr(mod, comp)
        self.botModules[name] = mod
        self.botModules[name].load(EH)
        return "Loaded: {0}!".format(name)

    def cmdloadModule(self, irc=None, msgInfo=None, args=None):
        if msgInfo['ident'] == "aacoba" and msgInfo['hostname'] == "aacoba.net":
            regex = re.compile("^!l (?P<name>\S+)", re.IGNORECASE)
            r = regex.search(msgInfo['message'])
            rgd = r.groupdict()
            return self.loadModule(rgd['name'])

    def reloadModule(self, irc=None, msgInfo=None, args=None):
        if msgInfo['ident'] == "aacoba" and msgInfo['hostname'] == "aacoba.net":
            regex = re.compile(r"!r (?P<name>\S+)", re.IGNORECASE)
            r = regex.search(msgInfo['message'])
            print r
            rgd = r.groupdict()
            name = rgd['name']
            print rgd['name']
            try:
                self.botModules[name].unload(EH)
            except KeyError:
                return "Module isn't loaded..."
            self.botModules[name] = __import__(name)
            self.botModules[name] = imp.reload(self.botModules[name])
            self.botModules[name].load(EH)
            return "Reloaded module: {0}!".format(name)

    def unloadModule(self, irc=None, msgInfo=None, args=None):
        if msgInfo['ident'] == "aacoba" and msgInfo['hostname'] == "aacoba.net":
            regex = re.compile(r"!u (?P<name>\S+)", re.IGNORECASE)
            r = regex.search(msgInfo['message'])
            rgd = r.groupdict()
            name = rgd['name']
            print rgd['name']
            try:
                self.botModules[name].unload(EH)
            except KeyError:
                return "Module isn't loaded..."
            self.botModules.pop(name)
            return "Unloaded module: {0}!".format(name)

    def listModules(self, irc=None, msgInfo=None, args=None):
        m = []
        for key, value in self.botModules.iteritems():
            m.append(key)
        if m is []:
            m = "No Modules Loaded!"
        return str(m)


class PermissionsHandler():
    def __init__(self):
        pass

    def checkAccess(self, msgInfo, command):
        pass


# EH = EventHandler()
# MH = ModuleHandler()
# EH.addHook("^!m", MH.listModules)
# EH.addHook("^!r (?P<name>\S+)", MH.reloadModule)
# EH.addHook("^!u (?P<name>\S+)", MH.unloadModule)
# EH.addHook("^!l (?P<name>\S+)", MH.cmdloadModule)
# EH.addHook("^!restartbot.*", irc.cmdRestart)
#
# irc = irc("irc.tweakers.net", "Iguana")
# time.sleep(2)
# DH = DataHandler()
# MH.loadModule("sampleModule")
# MH.loadModule("admin")
# MH.loadModule("Soul")
# MH.loadModule("Kelner")
# MH.loadModule("Maestro")
# irc.joinChannel("aacoba")
# irc.joinChannel("Hitsigepubers")
# irc.nickServAuth("leguaan")
#
#
# while True:
#     data = irc.con.recv(1024)
#     datalines = data.split("\r\n")
#     for line in datalines:
#         print line
#         thread.start_new_thread(DH.check, (line,))

EH = EventHandler()
EH.addHook(["channel", "^!hoi"], "handler")
EH.removeHook(["channel", "^!hoi"])
EH.addHook(["channel", "^!hoi"], "h0ax")
