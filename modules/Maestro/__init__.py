__author__ = 'auke'
import urllib2
import json
import random


def load(EH):
    EH.addHook("!hypem", giveHypemTrack)


def unload(EH):
    EH.removeHook("!hypem")


def giveHypemTrack(irc=None, msgInfo=None, args=None):
    jsonfile = urllib2.urlopen("http://hypem.com/playlist/popular/3day/json/1/data.js").read()
    tracks = json.loads(jsonfile)
    track = random.choice(tracks.values())
    try:
        return "{0} - {1}: http://hypem.com/go/sc/{2}".format(track['artist'], track['title'], track['mediaid'])
    except TypeError:
        if msgInfo['channel'] == "aacoba":
            irc.sendChannelMessage(msgInfo['channel'], "Track error: {0}".format(track))
        else:
            irc.sendUserMessage("aacoba", "Track error: {0}".format(track))
        print track
        return giveHypemTrack(irc, msgInfo, args)

