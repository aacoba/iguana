__author__ = 'auke'
import re


def load(EventHandler):
    EventHandler.addHook(r"^!raw (?P<command>.*)", sendRaw)
    EventHandler.addHook(r"^!help", bothelp)

def unload(EventHandler):
    EventHandler.removeHook(r"^!raw (?P<command>.*)")
    EventHandler.removeHook(r"^!help")

def bothelp(irc=None, msgInfo=None, args=None):
    return "Iguana: A Python IRC bot By aacoba(@aacoba.net)\r\nSourcecode: https://bitbucket.org/aacoba/iguana"

def sendRaw(irc=None, msgInfo=None, args=None):
    if msgInfo['ident'] == "aacoba" and msgInfo['hostname'] == "aacoba.net":
        message = msgInfo['message']
        regex = re.compile(r"!raw (?P<command>.*)", re.IGNORECASE)
        r = regex.search(message)
        rgd = r.groupdict()
        print rgd['command']
        irc.con.send(rgd['command']+"\r\n")
    else:
        return "Nope.avi"



