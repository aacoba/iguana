__author__ = 'auke'
import re
import random
import time


def load(EventHandler):
    EventHandler.addHook("^!cola", cola)
    EventHandler.addHook("^!melk", melk)
    EventHandler.addHook("^!bier", bier)
    EventHandler.addHook("^\:\(", roosvicee)
    EventHandler.addHook("^!sex", sex)
    EventHandler.addHook("^!zwembad", zwembad)
    EventHandler.addHook("^!dicht", dicht)
    EventHandler.addHook("^!dice", dice)
    EventHandler.addHook("^!emma", emma)
    EventHandler.addHook("emmie", emmiekick)
    EventHandler.addHook("^!(koekje|cookie)", koekje)
    EventHandler.addHook("^!mibbit", mibbit)


def unload(EventHandler):
    EventHandler.removeHook("^!cola")
    EventHandler.removeHook("^!melk")
    EventHandler.removeHook("^!bier")
    EventHandler.removeHook("^\:\(")
    EventHandler.removeHook("^!sex")
    EventHandler.removeHook("^!zwembad")
    EventHandler.removeHook("^!dicht")
    EventHandler.removeHook("^!dice")
    EventHandler.removeHook("^!emma")
    EventHandler.removeHook("emmie")
    EventHandler.removeHook("^!(koekje|cookie)")
    EventHandler.removeHook("^!mibbit")

def dicht(irc=None, msgInfo=None, args=None):
    emmaregex = re.compile(r"emma", re.IGNORECASE)
    r = emmaregex.search(msgInfo['user'])
    if r:
        return "Het is dict niet dicht..."
    else: pass


def cola(irc=None, msgInfo=None, args=None):
    lines = ["\001ACTION Geeft {0} een glaasje kindercola met een parasolletje en een rietje.\001"]
    lico = len(lines)
    n = random.randint(0, lico-1)
    return lines[n].format(msgInfo['user'])


def melk(irc=None, msgInfo=None, args=None):
    regex = re.compile(r"^!melk\s?(?P<target>\S*)", re.IGNORECASE)
    r = regex.search(msgInfo['message'])
    rgd = r.groupdict()
    if rgd['target'] is "":
        return "\001ACTION Geeft een glas verse melk (zonder klontjes!) aan {0}\001".format(msgInfo['user'])
    else:
        return "\001ACTION Geeft een glas verse melk (zonder klontjes!) aan {0}\001".format(rgd['target'])



def bier(irc=None, msgInfo=None, args=None):
    lines = ["\001ACTION Stuurt een leuke meid met een pul bier voor {0} naar {0}\001"]
    lico = len(lines)
    n = random.randint(0, lico-1)
    return lines[n].format(msgInfo['user'])


def roosvicee(irc=None, msgInfo=None, args=None):
    lines = ["\001ACTION Geeft {0} een glaasje roosvicee, het komt wel goed schatje\001"]
    lico = len(lines)
    n = random.randint(0, lico-1)
    return lines[n].format(msgInfo['user'])


def sex(irc=None, msgInfo=None, args=None):
    lines = ["Niet zo hopen {0}"]
    lico = len(lines)
    n = random.randint(0, lico-1)
    return lines[n].format(msgInfo['user'])


def zwembad(irc=None, msgInfo=None, args=None):
    return "Sorry {0}, het zwembadje is lek! D:".format(msgInfo['user'])

def koekje(irc=None, msgInfo=None, args=None):
    lines = ["\001ACTION Geeft een chocochip koekje aan {0}\001", "\001ACTION Geeft een pak heerlijke bastongekoekjes aan {0}\001", "\001ACTION propt {0} een gevulde koek in de snavel\001"]
    lico = len(lines)
    n = random.randint(0, lico-1)
    return lines[n].format(msgInfo['user'])


def dice(irc=None, msgInfo=None, args=None):
    regex = re.compile(r"!dice ?(?P<sides>\d*) ?(?P<throws>\d*)", re.IGNORECASE)
    r = regex.search(msgInfo['message'])
    rgd = r.groupdict()
    sides = rgd['sides']
    throws = rgd['throws']
    print rgd
    if sides is "" and throws is "":
        return "Dice: {0}".format(str(random.randint(1, 6)))
    elif sides is not "" and throws is "":
        return "Dice: {0}".format(str(random.randint(1, int(sides))))
    elif sides is not "" and throws is not "":
        throws = range(int(throws))
        results = []
        for throw in throws:
            results.append(random.randint(1, int(sides)))
        return "Dice: {0}".format(str(results))

def mibbit(irc=None, msgInfo=None, args=None):
    return "Mibbit is geen echte client!"


def emma(irc=None, msgInfo=None, args=None):
    return "\001ACTION Gebruikt alle mogelijke fu om Emma hierheen te Teleporten\001"

def emmiekick(irc=None, msgInfo=None, args=None):
    allowedNicks = ['aacoba', 'steephh']
    if msgInfo['user'].lower() not in allowedNicks:
        irc.sendChannelMessage(msgInfo['channel'], "BEEP BEEP, TERMINATE, TERMINATE, TERMINATE...")
        time.sleep(1)
        irc.con.send("KICK #{0} {1} :TERMINATE, TERMINATE, TERMINATE...\r\n".format(msgInfo['channel'], msgInfo['user']))