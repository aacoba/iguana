__author__ = 'auke'

import re
import random


def load(EventHandler):
    EventHandler.addHook("ACTION (aait|pets)", purr)
    pass


def unload(EventHandler):
    EventHandler.removeHook("ACTION (aait|pets)")
    pass


def purr(irc=None, msgInfo=None, args=None):
    regex = re.compile(r"^\001ACTION (aait|pets) (?P<target>\S+).*\001", re.IGNORECASE)
    r = regex.search(msgInfo['message'])
    if r is None:
        return
    rgd = r.groupdict()
    print [str.lower(rgd['target']), str.lower(irc.nickname)]
    if str.lower(rgd['target']) == str.lower(irc.nickname):
        lines = ["\001ACTION Purrs :3\001", "\001ACTION Prrrrrrrr :*\001", "\001ACTION Prrrrrrrr :*\001", "\001ACTION Prrrrrrrr :*\001"]
        lico = len(lines)
        n = random.randint(0, lico-1)
        return lines[n]
    else:
        print "Detected aai!, passed!"
