__author__ = 'auke'


def hoi(irc=None, msgInfo=None, args=None):
    return "Hoi"


def doei(irc=None, msgInfo=None, args=None):
    return "doei"


def variableMessage(irc=None, msgInfo=None, args=None):
    return msgInfo['message']


def load(EventHandler):
    EventHandler.addHook("!hoi", hoi)
    EventHandler.addHook("!doei", doei)
    EventHandler.addHook("!vm", variableMessage, "derp")



def unload(EventHandler):
    EventHandler.removeHook("!hoi")
    EventHandler.removeHook("!doei")
    EventHandler.removeHook("!vm")