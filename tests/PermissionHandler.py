__author__ = 'aacoba@aacoba.net'

import MySQLdb as mdb
import re
import time

#TODO: .'s Escapen, want . is elk character


con = mdb.connect('ftb.aacoba.net', 'Iguana', "eGqAz7Y7SBLbTCNt", "IguanaPermissions")


class PermissionHandler:
    def __init__(self):
        self.table = "TweakersPermissions" # not used ATM :+
        pass

    def buildQuery(self, bqDict):
        query = "SELECT * FROM `TweakersPermissions` WHERE (`nickname` = '{0}' or `nickname` = '*" \
                "') and (`ident` = '{1}' or `ident` = '*') and (`hostname` = '{2}' or `hostname` = '*') and (`channel` = '{3}" \
                "' or `channel` = '*')"
        print query.format(*bqDict)
        return query.format(*bqDict)

    def getPermissions(self, nickname, ident, hostname, channel, permission):
        query = self.buildQuery((nickname, ident, hostname, channel))
        cur = con.cursor()
        cur.execute(query)
        result = cur.fetchall()
        positiveNodes, negativeNodes, validNodes = [[], [], []]
        access = False
        for row in result:
            result = row[5]
            if result[0] == "-":
                negativeNodes.append(result.replace('-', ''))
            else:
                positiveNodes.append(result)
        for node in positiveNodes:
            if self.checkAccess(node, permission):
                validNodes.append(node)
                print "Matched positieve {} {}".format(node, permission)
                access = True
        for node in negativeNodes:
            if self.checkAccess(node, permission):
                access = False
                print "Matched negatieve {} {}".format("-" + node, permission)

        return access


    def checkAccess(self, given, needed):
        if given == "*":
            given = ".*"
        rGiven = re.compile(given, re.IGNORECASE)
        match = rGiven.match(needed)
        if match:
            print "True!, G:{} N:{}".format(given, needed)
            return True
        else:
            #print "False!, G:{} N:{}".format(given, needed)
            return False


if __name__ == "__main__":
    sTime = time.time()
    PH = PermissionHandler()
    print PH.getPermissions("negtest", "uberadmin", "aacoba.net", "cameraspying", "modules.kelner.derp")
    print "Took: {}".format(time.time() - sTime)