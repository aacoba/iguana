__author__ = 'aacoba@aacoba.net'


class ModuleException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)

